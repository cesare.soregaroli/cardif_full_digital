﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlClient;

namespace Cardif_Full_Digital
{

    public partial class Form1 : Form
    {
        private static readonly NLog.Logger nlog = NLog.LogManager.GetCurrentClassLogger();
        string applicazione;
        string percorso_file_protocollo_App = System.Configuration.ConfigurationManager.AppSettings["Path_Protocollo"].ToString();
        string percorso_file_protocollo = String.Empty;
        public Form1(string[] campi)
        {
            InitializeComponent();

            //            string percorso_file_protocollo = string.Empty;

            //Leggo l'applicazione
            foreach (string c in campi)
            {
                //Escludo il percorso dove salvare il file con il protocollo
                if (c.IndexOf("\\") != -1)
                {
                    percorso_file_protocollo = c;
                    continue;
                }

                //Escludo i valori da assegnare ai campi
                if (c.IndexOf("=") != -1)
                    continue;

                applicazione = c.ToUpper();
                //break;
            }

            //Valorizzo il dictionary per inserire i parametri nei rispettivi campi
            Dictionary<string, string> campi_indicizzazione = Valorizza_campi_indicizzazione();

            //Valorizzo le combobox
            Valorizza_tipo_posta();
            Valorizza_destinatario();
            Valorizza_tipo_lettera();

            //Inserisco i valori predefiniti delle textbox
            Valorizza_data_ricevimento();

            //Leggo i valori passati e valorizzo i campi
            Valorizza_campi(campi_indicizzazione, campi);

            //Chiedo il nuovo protocollo
            //            Chiedi_Protocollo(percorso_file_protocollo);
        }

        CardifClass.DatiContextClass myServiceDataClass = new CardifClass.DatiContextClass();

        private void Valorizza_tipo_posta()
        {
            #region POSTA
            cb_tipo_posta.Items.Add(string.Empty);
            cb_tipo_posta.Items.Add("Posta Ordinaria - 1");
            cb_tipo_posta.Items.Add("Raccomandata - 2");
            cb_tipo_posta.Items.Add("Fax - 3");
            cb_tipo_posta.Items.Add("Ricevuta Raccomandata - 4");
            cb_tipo_posta.Items.Add("Corriere - 7");
            cb_tipo_posta.Items.Add("Cons. a mano - 8");
            #endregion
            #region PNR
            cb_tipo_posta.Items.Add("PNR Posta Ordinaria - 5");
            cb_tipo_posta.Items.Add("PNR Raccomandata - 6");
            cb_tipo_posta.Items.Add("PNR Corriere - 9");
            cb_tipo_posta.Items.Add("PNR Cons. a mano - 10");
            #endregion
        }

        private void Valorizza_destinatario()
        {
            cb_destinatario.Items.Add(string.Empty);
            cb_destinatario.Items.Add("Gestione Risparmio - 1");
            cb_destinatario.Items.Add("Gestione Linea Persone - 12");
            cb_destinatario.Items.Add("Cqs - 18");
            cb_destinatario.Items.Add("Ufficio Sinistri - 19");
        }

        private void Valorizza_tipo_lettera()
        {
            cb_tipo_lettera.Items.Add(string.Empty);
            cb_tipo_lettera.Items.Add("Lettera NON Recapitata - 1");
            cb_tipo_lettera.Items.Add("Posta Personale - 3");
        }

        private void cb_destinatario_TextChanged(object sender, EventArgs e)
        {
            //Rimuovo i valori della combobox CODICE_AREA e COD_PARTNER
            cb_codice_area.Items.Clear();
            cb_cod_partner.Items.Clear();

            //Pulisco i campi CODICE_AREA e COD_PARTNER
            cb_codice_area.Text = string.Empty;
            cb_cod_partner.Text = string.Empty;

            //Inserisco i valori  della combobox CODICE_AREA e COD_PARTNER in base al valore della combobox DESTINATARIO
            switch (cb_destinatario.Text)
            {
                case "Gestione Risparmio - 1":
                    #region CODICE_AREA
                    cb_codice_area.Items.Add(string.Empty);
                    cb_codice_area.Items.Add("RECLAMI - 12");
                    cb_codice_area.Items.Add("FPA-PARVEST - 16");
                    cb_codice_area.Items.Add("VITA_IDM - 19");
                    cb_codice_area.Items.Add("VITA_DIR TEC - 20");
                    cb_codice_area.Items.Add("VITA_DIR FIN - 21");
                    cb_codice_area.Items.Add("VITA_AFFARI SOCIETARI - 22");
                    cb_codice_area.Items.Add("VITA_SEGRETERIA GENERALE - 23");
                    cb_codice_area.Items.Add("VITA_RISORSE UMANE - 24");
                    cb_codice_area.Items.Add("PREVIDENZA - 27");
                    cb_codice_area.Items.Add("ATTI RISPARMIO - 28");
                    cb_codice_area.Items.Add("ATTI SAVING - 29");
                    cb_codice_area.Items.Add("RECLAMI RISPARMIO - 30");
                    cb_codice_area.Items.Add("RECLAMI SAVING - 31");
                    cb_codice_area.Items.Add("SAVING - 32");
                    #endregion
                    //aggiunto riga COD_PARTNER per test
                    #region COD_PARTNER 
                    cb_cod_partner.Items.Add(string.Empty);
                    #endregion

                    break;

                case "Gestione Linea Persone - 12":

                    #region CODICE_AREA
                    cb_codice_area.Items.Add(string.Empty);
                    cb_codice_area.Items.Add("ASSUNZIONE - DBS/MdA - 1");
                    cb_codice_area.Items.Add("SINISTRI - DENUNCIA - 2");
                    cb_codice_area.Items.Add("SINISTRI - SEGUITO - 3");
                    cb_codice_area.Items.Add("RECLAMI - 4");
                    cb_codice_area.Items.Add("POST VENDITA - 5");
                    cb_codice_area.Items.Add("ASSUNZIONE - QM/VM - 6");
                    cb_codice_area.Items.Add("SEGRETERIA GENERALE - 15");
                    cb_codice_area.Items.Add("FATTURE - 17");
                    cb_codice_area.Items.Add("ATTI DI CITAZIONE - 18");
                    cb_codice_area.Items.Add("CPI INDIVIDUALE FINDO - 25");
                    cb_codice_area.Items.Add("TCM FINDOMESTIC - 26");
                    #endregion
                    #region COD_PARTNER
                    cb_cod_partner.Items.Add(string.Empty);
                    cb_cod_partner.Items.Add("CENTROVITA - 0");
                    cb_cod_partner.Items.Add("CONSEL - 111");
                    cb_cod_partner.Items.Add("FINDOMESTIC - 151");
                    cb_cod_partner.Items.Add("BARCLAYS - 196");
                    cb_cod_partner.Items.Add("AGOS - 504");
                    cb_cod_partner.Items.Add("CR FIRENZE - 506");
                    cb_cod_partner.Items.Add("COMPASS - 528");
                    cb_cod_partner.Items.Add("MICOS - 531");
                    cb_cod_partner.Items.Add("BNL - 532");
                    cb_cod_partner.Items.Add("UNICREDIT - 550");
                    cb_cod_partner.Items.Add("BNL FINANCE - 555");
                    cb_cod_partner.Items.Add("BANCA MARCHE - 599");
                    cb_cod_partner.Items.Add("CARILO - 600");
                    #endregion

                    break;

                case "Cqs - 18":
                    #region CODICE_AREA
                    cb_codice_area.Items.Add(string.Empty);
                    cb_codice_area.Items.Add("ASSUNZIONE - 7");
                    cb_codice_area.Items.Add("SINISTRI - 8");
                    cb_codice_area.Items.Add("RECUPERI - 9");
                    cb_codice_area.Items.Add("POST VENDITA - 10");
                    cb_codice_area.Items.Add("RECLAMI - 11");
                    #endregion
                    #region COD_PARTNER
                    cb_cod_partner.Items.Add(string.Empty);
                    cb_cod_partner.Items.Add("BNL FINANCE - 995");
                    cb_cod_partner.Items.Add("SIGLA - 996");
                    cb_cod_partner.Items.Add("MYTHOS - 997");
                    cb_cod_partner.Items.Add("CATTOLICA - 998");
                    cb_cod_partner.Items.Add("BF5 - 999");
                    #endregion

                    break;

                case "Ufficio Sinistri - 19":
                    #region CODICE_AREA
                    cb_codice_area.Items.Add(string.Empty);
                    cb_codice_area.Items.Add("SINISTRI - DENUNCIA - 13");
                    cb_codice_area.Items.Add("SINISTRI – SEGUITO - 14");
                    #endregion
                    #region COD_PARTNER
                    cb_cod_partner.Items.Add(string.Empty);
                    cb_cod_partner.Items.Add("CENTROVITA - 0");
                    cb_cod_partner.Items.Add("CONSEL - 111");
                    cb_cod_partner.Items.Add("FINDOMESTIC - 151");
                    #endregion

                    break;
            }
        }

        private void Valorizza_data_ricevimento()
        {
            txt_dt_ricevimento.Text = DateTime.Now.ToShortDateString();
        }

        private void bt_richiesta_Click(object sender, EventArgs e)
        {
            if (Controlli())
            {

            }
        }

        private bool Controlli()
        {
            try
            {
                #region DT_RICEVIMENTO
                if (txt_dt_ricevimento.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo DT_RICEVIMENTO è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_dt_ricevimento.Focus();
                    return false;
                }
                else if (DateTime.ParseExact(txt_dt_ricevimento.Text.Trim(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) > DateTime.Now)
                {
                    MessageBox.Show("la data inserita nel campo DT_RICEVIMENTO non può essere nel futuro", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_dt_ricevimento.Focus();
                    return false;
                }
                else
                    myServiceDataClass.DtRicevimento = DateTime.Parse(txt_dt_ricevimento.Text);
                #endregion

                #region TIPO_POSTA
                if (cb_tipo_posta.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo TIPO_POSTA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_tipo_posta.Focus();
                    return false;
                }
                else
                    myServiceDataClass.TpPosta = CardifClass.Converter.ReturnValue(cb_tipo_posta.Text);

                #endregion

                #region DESTINATARIO
                if (cb_destinatario.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo DESTINATARIO è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_destinatario.Focus();
                    return false;
                }
                else
                    myServiceDataClass.Destinatario = CardifClass.Converter.ReturnValue(cb_destinatario.Text);

                #endregion

                #region NUM_PAGINE
                //if (!int.TryParse(txt_num_pagine.Text.Trim(), out int n))
                //{
                //    MessageBox.Show("il campo NUM_PAGINE deve essere valorizzato con un numero", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    txt_num_pagine.Focus();
                //    return false;
                //}
                //else
                //    myServiceDataClass.NumPagine=CardifClass.Converter.ReturnValue(txt_num_pagine.Text);

                if (!int.TryParse(CmbNPag.Text.Trim(), out int n))
                {
                    MessageBox.Show("il campo NUM_PAGINE deve essere valorizzato con un numero", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CmbNPag.Focus();
                    return false;
                }
                else
                    myServiceDataClass.NumPagine = CardifClass.Converter.ReturnValue(CmbNPag.Text);

                #endregion

                #region NUM_RACCOMANDATA
                if (new List<string>() { "Raccomandata - 2", "Ricevuta Raccomandata - 4", "PNR Raccomandata - 6" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_num_raccomandata.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo NUM_RACCOMANDATA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_num_raccomandata.Focus();
                        return false;
                    }
                    else //modifiche luigi 21/05/2019
                        myServiceDataClass.Raccomandata = txt_num_raccomandata.Text.ToString();
                }
                else if (txt_num_raccomandata.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo NUM_RACCOMANDATA non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_num_raccomandata.Focus();
                    return false;
                }
                else
                    myServiceDataClass.Raccomandata = txt_num_raccomandata.Text.ToString();

                #endregion


                #region TIPO_LETTERA
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (cb_tipo_lettera.Text.Trim().Length != 0)
                    {
                        MessageBox.Show("il campo TIPO_LETTERA non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cb_tipo_lettera.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.TpLettera = CardifClass.Converter.ReturnValue(cb_tipo_lettera.Text);
                }
                else if (cb_tipo_lettera.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo TIPO_LETTERA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_tipo_lettera.Focus();
                    return false;
                }
                else
                    myServiceDataClass.TpLettera = CardifClass.Converter.ReturnValue(cb_tipo_lettera.Text);
                #endregion

                #region MITTENTE
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_mittente.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo MITTENTE è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_mittente.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.Mittente = txt_mittente.Text;

                }
                else if (txt_mittente.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo MITTENTE non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_mittente.Focus();
                    return false;
                }
                else
                    myServiceDataClass.Mittente = txt_mittente.Text;

                #endregion

                #region DT_INVIO
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_dt_invio.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo DT_INVIO è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_dt_invio.Focus();
                        return false;
                    }
                    else if (DateTime.ParseExact(txt_dt_invio.Text.Trim(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) > DateTime.Now)
                    {
                        MessageBox.Show("la data inserita nel campo DT_INVIO non può essere nel futuro", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_dt_invio.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.DtInvio = DateTime.Parse(txt_dt_invio.Text);
                }
                else if (txt_dt_invio.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo DT_INVIO non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_dt_invio.Focus();
                    return false;
                }
                #endregion

                #region CODICE_AREA
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (cb_codice_area.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo CODICE_AREA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cb_codice_area.Focus();
                        return false;
                    }
                    else
                    {
                        myServiceDataClass.CodArea = CardifClass.Converter.ReturnValue(cb_codice_area.Text);
                    }
                }
                else if (cb_codice_area.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo CODICE_AREA non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_codice_area.Focus();
                    return false;
                }
                #endregion

                #region COMMENTO
                if (!new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_commento.Text.Trim().Length != 0)
                    {
                        MessageBox.Show("il campo COMMENTO non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_commento.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.Comment = txt_commento.Text;
                }
                else
                    myServiceDataClass.Comment = string.Empty;

                #endregion

                #region COD_PARTNER
                if (!new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (cb_cod_partner.Text.Trim().Length != 0)
                    {
                        MessageBox.Show("il campo COD_PARTNER non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cb_cod_partner.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.Partner = CardifClass.Converter.ReturnValuestring(cb_cod_partner.Text);

                }
                else  ///modifica 23/05/2019 a seguito errore provocato da  cod partner=0, in CodPartner = "" combio del tipo dato della classe da Int a string
                {
                    myServiceDataClass.Partner = string.Empty;

                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private Dictionary<string, string> Valorizza_campi_indicizzazione()
        {
            Dictionary<string, string> campi = new Dictionary<string, string>();

            foreach (Control c in panel1.Controls)
            {
                if (c is Label)
                {
                    if (panel1.Controls.ContainsKey("txt_" + c.Text.ToLower()))
                        campi.Add(c.Text.ToLower(), "txt_" + c.Text.ToLower());
                    else if (panel1.Controls.ContainsKey("cb_" + c.Text.ToLower()))
                        campi.Add(c.Text.ToLower(), "cb_" + c.Text.ToLower());
                }
            }
            return campi;
        }

        private void Valorizza_campi(Dictionary<string, string> campi_indicizzazione, string[] campi)
        {
            foreach (string c in campi)
            {
                if (c.IndexOf("=") == -1)
                    continue;

                string[] campi_split = c.ToLower().Split('=');

                if (campi_indicizzazione.ContainsKey(campi_split[0]))
                    panel1.Controls.Find(campi_indicizzazione[campi_split[0]], true).First().Text = campi_split[1].ToUpper();
                else MessageBox.Show("Campo " + campi_split[0].ToUpper() + " non trovato!", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void Chiedi_Protocollo(string percorso)
        {
            nlog.Info("Inizio della chiamata Chiedi_Protocollo");
            string nprotocollo = string.Empty;

            try
            {

                //using (WebServiceCardifnew.PPWS2ProxyServiceClient WsCardif = new WebServiceCardifnew.PPWS2ProxyServiceClient())
                //{
                WebServiceCardifnew.PPWS2ProxyServiceClient WsCardif = new WebServiceCardifnew.PPWS2ProxyServiceClient();

                System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, error) =>
            {
                return true;
            };


                // string CardifUrl = Properties.Settings.Default.myUrl.ToString();
                string CardifUrl = System.Configuration.ConfigurationManager.AppSettings["WS_EndPoint"].ToString();
                WsCardif.Endpoint.Address = new System.ServiceModel.EndpointAddress(CardifUrl);

                //nlog.Info($"Chiamata Servizio {CardifUrl}");

                //"https://10.238.17.106/9029/service/protocollazionePosta/v1"
                //                    WsCardif.Endpoint.Address = new System.ServiceModel.EndpointAddress("https://192.168.62.51/9029/service/protocollazionePosta/v1");

                //Basic Autentication - Parametri Utilizzati per autenticare il client
                //WsCardif.ClientCredentials.UserName.UserName = "techusr_microdata";
                //WsCardif.ClientCredentials.UserName.Password = "techusr_microdata";
                WsCardif.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings["WS_Utente_Basic"].ToString();
                WsCardif.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings["WS_Password_Basic"].ToString();

                //nlog.Info($"Servizio Username {WsCardif.ClientCredentials.UserName.UserName}");
                //nlog.Info($"Servizio Password {WsCardif.ClientCredentials.UserName.Password}");

                //WsCardif.ClientCredentials.ServiceCertificate.SetDefaultCertificate(StoreLocation.LocalMachine, StoreName.My, X509FindType.FindBySubjectName, "mioCert");

                WebServiceCardifnew.getNumProtocolRequest dati = new WebServiceCardifnew.getNumProtocolRequest();
                nlog.Info($"Inizio Dati da salvare");
                //dati.
                dati.DT_RICEVIMENTO = myServiceDataClass.DtRicevimento;
                nlog.Info($"dati.DT_RICEVIMENTO  - {dati.DT_RICEVIMENTO} -");
                dati.CODICE_AREA = myServiceDataClass.CodArea;
                nlog.Info($"dati.CODICE_AREA  - {dati.CODICE_AREA} -");
                dati.TIPO_POSTA = myServiceDataClass.TpPosta;
                nlog.Info($"dati.TIPO_POSTA  - {dati.TIPO_POSTA} -");
                dati.DESTINATARIO = myServiceDataClass.Destinatario;
                nlog.Info($"dati.DESTINATARIO  - {dati.DESTINATARIO} -");
                dati.NUM_PAGINE = myServiceDataClass.NumPagine;
                nlog.Info($"dati.NUM_PAGINE  - {dati.NUM_PAGINE} -");
                dati.NUM_RACCOMANDATA = myServiceDataClass.Raccomandata;
                nlog.Info($"dati.NUM_RACCOMANDATA  - {dati.NUM_RACCOMANDATA} -");
                dati.TIPO_LETTERA = myServiceDataClass.TpLettera;
                nlog.Info($"dati.TIPO_LETTERA  - {dati.TIPO_LETTERA} -");
                dati.MITTENTE = myServiceDataClass.Mittente;
                nlog.Info($"dati.MITTENTE  - {dati.MITTENTE} -");
                dati.DT_INVIO = myServiceDataClass.DtInvio;
                nlog.Info($"dati.DT_INVIO  - {dati.DT_INVIO} -");
                dati.COMMENTO = myServiceDataClass.Comment;
                nlog.Info($"dati.COMMENTO  - {dati.COMMENTO} -");
                dati.COD_PARTNER = myServiceDataClass.Partner;
                nlog.Info($"dati.COD_PARTNER  - {dati.COD_PARTNER} -");

                nlog.Info($"Fine Dati da salvare");

                //Parametri di login per qquanto riguarda il WSDL
                WebServiceCardifnew.SecurityInfoTypeUserInfo WsLogin = new WebServiceCardifnew.SecurityInfoTypeUserInfo();
                //                   WsCardif.Open();
                //werQ1234
                WsLogin.Login = System.Configuration.ConfigurationManager.AppSettings["WS_Utente"].ToString();
                WsLogin.Password = System.Configuration.ConfigurationManager.AppSettings["WS_Password"].ToString();

                WebServiceCardifnew.HeaderInput WsHeader = new WebServiceCardifnew.HeaderInput();

                WsHeader.Header = new WebServiceCardifnew.HeaderInputType();
                WsHeader.Header.ContextPath = "";
                //                    WsHeader.Header.CorrelationId = "";
                WsHeader.Security = new WebServiceCardifnew.SecurityInfoType();
                WsHeader.Security.ApplicationId = "";
                WsHeader.Security.Item = WsLogin;

                WebServiceCardifnew.getNumProtocolResult result;
                try
                {
                    result = WsCardif.getNumProtocol(WsHeader, dati);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Chiamata al servizio per la protocollazione non effettuata correttamente. Contattare Ufficio IT per verifiche", "WS PROTOCOLLAZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    nlog.Error(ex.Message);
                    throw;
                }

                Int32 Protocollo = result.NUMPROTOCOL;
                //nlog.Info($"Num. Protocollo Tornato dal servizio - {Protocollo} -");
                //                    WsCardif.Close();
                WsCardif.Close();
                //result.
                string ApplicationID = Guid.NewGuid().ToString();


                //if (Protocollo < 0)  // minore di 0 segnala un errore
                //{
                //    nlog.Info($"Num. Protocollo Inferiore a 0 - {Protocollo} -");
                //    if (!string.IsNullOrEmpty(result.ERRORS))
                //    {
                //        TxtError.Text = result.ERRORS;
                //        nlog.Info($"Errore - {result.ERRORS} -");
                //    }
                //    else
                //        TxtError.Text = CardifClass.RetriveError.ReturnMsgError(result.NUMPROTOCOL);
                //    //Mostra il messaggio di errore
                //}

                if (!string.IsNullOrEmpty(result.ERRORS) || Protocollo < 0)
                {

                    MessageBox.Show("Errore nella Chiamata del Servizio. Contattare Ufficio IT per verifiche", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    TxtError.Text = result.ERRORS;
                    nlog.Error($"Errore - {result.ERRORS}");

                }
                else
                {
                    Int32 nProtocollo = result.NUMPROTOCOL;
                    
                    TxtProtocol.Text = nProtocollo.ToString();
                    TxtError.Text = string.Empty;
                    //Effettuo questa modifica perchè in caso di errore del programma 
                    //non deve inserire il codice nel campo Protocollo e nemmeno nel file di salvataggio                    
                    try
                    {
                        Clipboard.SetText(Protocollo.ToString());
                    }
                    catch (Exception)
                    {
                        TxtProtocol.Text = string.Empty;
                        throw new Exception("Errore salvataggio nella clipBoard del numero di Protocollo.Protocollo non Valido");
                    }
                    
                    //using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(percorso, "Protocollo.txt")))                    
                    nlog.Info($" INIZIO Stampa protocollo in file");
                    nlog.Info($" Num. Protocollo Tornato dal servizio - {nProtocollo} -");
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(percorso_file_protocollo_App, "Protocollo.txt")))
                    {
                        sw.Write(Protocollo);
                    }
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(percorso, "Protocollo.txt")))
                    {
                        sw.Write(Protocollo);
                    }

                    nlog.Info($" FINE Stampa protocollo in file");
                    
                    nlog.Info($" INIZIO Salvataggio a DataBase del record");
                    var Wr = new ConnectDb();
                    Wr.WriteTable(dati, nProtocollo);
                    nlog.Info($" FINE Salvataggio a DataBase del record");
                    ClearFields();
                    Clipboard.SetText(Protocollo.ToString());
                }
            }
            //}
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                nlog.Error(ex.Message);
                nlog.Error(ex.StackTrace);
                //Metto l'errore come se l'avesse copiato l'operatore
                //Clipboard.SetText("ERRORE");
            }

            nlog.Info("Fine della chiamata Chiedi_Protocollo");
        }

        private void ClearFields()
        {
            if (cb_tipo_posta.Items.Count > 0)
                cb_tipo_posta.SelectedIndex = 0;

            if (cb_destinatario.Items.Count > 0)
                cb_destinatario.SelectedIndex = 0;

            CmbNPag.Text = string.Empty;
            //txt_num_pagine.Text = string.Empty;
            txt_num_raccomandata.Text = string.Empty;

            if (cb_tipo_lettera.Items.Count > 0)
                cb_tipo_lettera.SelectedIndex = 0;

            txt_mittente.Text = string.Empty;
            //                txt_dt_invio.Text = string.Empty;

            if (cb_codice_area.Items.Count > 0)
                cb_codice_area.SelectedIndex = 0;

            txt_commento.Text = string.Empty;

            if (cb_cod_partner.Items.Count > 0)
                cb_cod_partner.SelectedIndex = 0;

            //TxtProtocol.Text = string.Empty;


        }

        private void bt_richiesta_Click_1(object sender, EventArgs e)
        {
            bool _ControlOk = Controlli();

            if (_ControlOk == true)
            {

                Chiedi_Protocollo(percorso_file_protocollo);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AfterSelected(object sender, EventArgs e)
        {
            string _nomeCombo = ((ComboBox)sender).Name.ToLower();

            ///modifica richiesta 174097 del 21/06/2019
            Int32 Idsel = ((ComboBox)sender).SelectedIndex;
            switch (Idsel)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    txt_dt_invio.Text = DateTime.Today.ToString("dd/MM/yyyy"); ;
                    cb_destinatario.Focus();
                    break;
                default:
                    txt_dt_invio.Text = string.Empty;
                    break;

            }






            switch (_nomeCombo)
            {
                case "cb_tipo_lettera":
                    cb_destinatario.Focus();
                    break;
                case "cb_tipo_posta":
                    CmbNPag.Focus();
                    break;
                case "cb_destinatario":
                    cb_tipo_posta.Focus();
                    break;
            }

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            try
            {
                Dispose();
                Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        //private void txt_dt_ricevimento_TextChanged(object sender, EventArgs e)
        //{

        //}
    }
    public class ConnectDb
    {
        private static readonly NLog.Logger nlog = NLog.LogManager.GetCurrentClassLogger();
        public void WriteTable(WebServiceCardifnew.getNumProtocolRequest Dati, int Nprotocollo)
        {
            Properties.Settings mySet = new Properties.Settings();
            string ConnectDb = System.Configuration.ConfigurationManager.ConnectionStrings["ConnDBCardif"].ToString();
            SqlCommand SQLCmd = new SqlCommand();
            string _RetPathFile = string.Empty;
            int _esito = 0;
            SqlConnection Connect = new SqlConnection(ConnectDb);
            try
            {
                Connect.Open();
                if (Connect.State == ConnectionState.Open)
                {
                    SQLCmd.Connection = Connect;
                    string SQLStr = @"INSERT INTO Protocolli_Fulldigital
                      (Protocollo, DataRicevimento, CodiceArea, TipoPosta, Destinatario, NumPagine, NumRaccomandata, TipoLettera, Mittente, DataInvio, Commento, CodPartner)
                      VALUES (@protocollo,@data,@area,@Tposta,@destinatario,@Numpag,@NRaccomandata,@TpLettera,@Mittente,@Datainvio,@Commento,@CdParner)";

                    SQLCmd.CommandText = SQLStr;
                    SQLCmd.Parameters.AddWithValue("@protocollo", Nprotocollo);
                    SQLCmd.Parameters.AddWithValue("@data", Dati.DT_RICEVIMENTO);
                    SQLCmd.Parameters.AddWithValue("@area", Dati.CODICE_AREA);
                    SQLCmd.Parameters.AddWithValue("@Tposta", Dati.TIPO_POSTA);
                    SQLCmd.Parameters.AddWithValue("@destinatario", Dati.DESTINATARIO);
                    SQLCmd.Parameters.AddWithValue("@Numpag", Dati.NUM_PAGINE);
                    SQLCmd.Parameters.AddWithValue("@NRaccomandata", Dati.NUM_RACCOMANDATA);
                    SQLCmd.Parameters.AddWithValue("@TpLettera", Dati.TIPO_LETTERA);
                    SQLCmd.Parameters.AddWithValue("@Mittente", Dati.MITTENTE);
                    SQLCmd.Parameters.AddWithValue("@Datainvio", Dati.DT_INVIO);
                    SQLCmd.Parameters.AddWithValue("@Commento", Dati.COMMENTO);
                    SQLCmd.Parameters.AddWithValue("@CdParner", Dati.COD_PARTNER);
                    _esito = SQLCmd.ExecuteNonQuery();


                    if (_esito < 0) { nlog.Error($"Salvataggio non effettuato correttamente. Valori Protocollo : { Nprotocollo} MITTENTE : {Dati.MITTENTE} DESTINATARIO : {Dati.DESTINATARIO} PARTNER : {Dati.COD_PARTNER}"); }
                    else { nlog.Info($"Salvataggio effettuato correttamente per il protocollo{Nprotocollo}"); }
                }
                else { nlog.Error($"Connessione al Database non aperta.{ConnectDb}"); }

            }
            catch (SqlException Ex)
            {
                MessageBox.Show(Ex.Message, "Salvataggio Dati");
                nlog.Error(Ex.Message);
                nlog.Error(Ex.StackTrace);
                throw;
            }
            finally { Connect.Close(); Connect.Dispose(); }

        }

    }

}
