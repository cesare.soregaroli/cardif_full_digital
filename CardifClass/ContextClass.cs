﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Cardif_Full_Digital.CardifClass
{
    public  class DatiContextClass
    {
        public DateTime DtRicevimento { get; set; }
        public Int32 TpPosta { get; set; } 
        public Int32 Destinatario { get; set; }
        public Int32 NumPagine { get; set; }
        public string Raccomandata { get; set; } 
        public Int32 TpLettera { get; set; } 
        public string Mittente { get; set; } 
        public  DateTime DtInvio { get; set; }
        public Int32 CodArea { get; set; } 
        public  string Comment { get; set; }
//        public  Int32 Partner { get; set; }
        public string Partner { get; set; }

    }
    public class Converter
    {
        /// <summary>
        /// versione del 31/05/2019 per il in codice partner tipo stringa
        /// </summary>
        /// <param name="TestoRicerca"></param>
        /// <returns></returns>
        public static string ReturnValuestring(string TestoRicerca)
        {
            Int32 RetValue = 0;
            Regex regex = new Regex("[0-9]+");
            string Str = TestoRicerca;
            Match match = regex.Match(Str);

            if (match.Success)
                RetValue = Int32.Parse(match.Value);

            return RetValue.ToString();

        }
        /// <summary>
        /// Versione del 30/05/2019 con Cod Partner numerico
        /// </summary>
        /// <param name="TestoRicerca"></param>
        /// <returns></returns>
        public static Int32 ReturnValue(string TestoRicerca)
        {
            Int32 RetValue = 0;

            Regex regex = new Regex("[0-9]+");
            string Str = TestoRicerca;
            Match match = regex.Match(Str);

            if (match.Success)
                RetValue = Int32.Parse(match.Value);


            return RetValue;

        }
    }
    internal class RetriveError
    {
        /// <summary>
        /// ritorna la descrizione della tipologia di errore legato al valore minore di 0 
        /// </summary>
        /// <param name="TpError"></param>
        /// <returns></returns>
        internal static string ReturnMsgError(Int32 TpError) 
        {
            string retValue = string.Empty;
            switch (TpError)
            {
                case -1:
                    retValue = "Si sono verificati uno o più errori riguardanti la validità del contenuto dei campi";
                    break;
                case -2:
                    retValue = "Si è verificato un errore applicativo";
                    break;
                case -3:
                    retValue = "Si sono verificati errori sul contenuto dei campi e applicativo";
                    break;
            }
            return retValue;

        }


    }


    public class ListVariable
    {
        public Int32 IdDest { get; set; }
        public Int32 IdFunction { get; set; }
        public string DeFunction { get; set; }

        public List<ListVariable> LoadFunzioniTipoPosta()
        {
            var TpPosta = new List<ListVariable>();
            TpPosta.Add(new ListVariable { IdDest = 0, IdFunction = 1, DeFunction = "Posta Ordinaria - 1" });
            TpPosta.Add(new ListVariable { IdDest = 0, IdFunction = 2, DeFunction = "FPA-Raccomandata - 2" });
            TpPosta.Add(new ListVariable { IdDest = 0, IdFunction = 3, DeFunction = "FAX - 3" });
            TpPosta.Add(new ListVariable { IdDest = 0, IdFunction = 4, DeFunction = "Ricevuta Raccomandata - 4" });
            TpPosta.Add(new ListVariable { IdDest = 0, IdFunction = 7, DeFunction = "Corriere - 7" });
            TpPosta.Add(new ListVariable { IdDest = 0, IdFunction = 8, DeFunction = "Consegna a mano - 8" });
            TpPosta.Add(new ListVariable { IdDest = 1, IdFunction = 5, DeFunction = "Posta Ordinaria PNR 5" });
            TpPosta.Add(new ListVariable { IdDest = 1, IdFunction = 6, DeFunction = "Raccomandata PNR 6" });
            TpPosta.Add(new ListVariable { IdDest = 1, IdFunction = 9, DeFunction = "Corriere PNR - 9" });
            TpPosta.Add(new ListVariable { IdDest = 1, IdFunction = 10, DeFunction = "Consegna a mano PNR - 10" });

            return TpPosta;
        }

        public List<ListVariable> LoadFunzioniArea()
        {
            var ListaArea = new List<ListVariable>();
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 12, DeFunction = "RECLAMI" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 16, DeFunction = "FPA-PARVEST" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 19, DeFunction = "VITA_IDM" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 20, DeFunction = "VITA-DIR-TEC" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 21, DeFunction = "VITA DIR FIN" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 22, DeFunction = "VITA AFFARI SOCIETARI" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 23, DeFunction = "VITA SEGRETERIA GENERALE" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 24, DeFunction = "VITA RISORSE UMANE" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 27, DeFunction = "PREVIDENZA" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 28, DeFunction = "ATTI RISPARMIO" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 29, DeFunction = "ATTI SAVING" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 30, DeFunction = "RECLAMI RISPARMIO" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 31, DeFunction = "RECLAMI SAVING" });
            ListaArea.Add(new ListVariable { IdDest = 1, IdFunction = 32, DeFunction = "SAVING" });

            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 1, DeFunction = "ASSUNZIONE DBS/MdA" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 2, DeFunction = "SINISTRI DENUNCIA" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 3, DeFunction = "SINISTRI SEGUITO" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 4, DeFunction = "RECLAMI" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 5, DeFunction = "POST VENDITA" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 6, DeFunction = "ASSUNZIONE QM/VM" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 15, DeFunction = "SEGRETERIA GENERALE" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 17, DeFunction = "FATTURE" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 18, DeFunction = "ATTI DI CITAZIONE" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 25, DeFunction = "CPI INDIVIDUALE FINDO" });
            ListaArea.Add(new ListVariable { IdDest = 12, IdFunction = 26, DeFunction = "TCM FINDOMESTIC" });

            ListaArea.Add(new ListVariable { IdDest = 18, IdFunction = 7, DeFunction = "ASSUNZIONE" });
            ListaArea.Add(new ListVariable { IdDest = 18, IdFunction = 8, DeFunction = "SINISTRI" });
            ListaArea.Add(new ListVariable { IdDest = 18, IdFunction = 9, DeFunction = "RECUPERI" });
            ListaArea.Add(new ListVariable { IdDest = 18, IdFunction = 10, DeFunction = "POST VENDITA" });
            ListaArea.Add(new ListVariable { IdDest = 18, IdFunction = 11, DeFunction = "RECLAMI" });

            ListaArea.Add(new ListVariable { IdDest = 19, IdFunction = 13, DeFunction = "SINISTRI DENUNCIA" });
            ListaArea.Add(new ListVariable { IdDest = 19, IdFunction = 14, DeFunction = "SINISTRI SEGUITO" });

            return ListaArea;
        }
        internal List<ListVariable> LoadFunzioniPartner()
        {
            var ListaPartner = new List<ListVariable>();

            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 0, DeFunction = "CENTROVITA" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 111, DeFunction = "CONSEL" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 151, DeFunction = "FINDOMESTIC" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 196, DeFunction = "BARCLAYS" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 504, DeFunction = "AGOS" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 506, DeFunction = "CR FIRENZE" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 528, DeFunction = "COMPASS" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 531, DeFunction = "MICOS" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 532, DeFunction = "BNL" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 550, DeFunction = "UNICREDIT" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 555, DeFunction = "BNL FINANCE" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 599, DeFunction = "BANCA MARCHE" });
            ListaPartner.Add(new ListVariable { IdDest = 12, IdFunction = 600, DeFunction = "CARILO" });

            ListaPartner.Add(new ListVariable { IdDest = 18, IdFunction = 995, DeFunction = "BNL FINANCE" });
            ListaPartner.Add(new ListVariable { IdDest = 18, IdFunction = 996, DeFunction = "SIGLA" });
            ListaPartner.Add(new ListVariable { IdDest = 18, IdFunction = 997, DeFunction = "MYTHOS" });
            ListaPartner.Add(new ListVariable { IdDest = 18, IdFunction = 998, DeFunction = "CATTOLICA" });
            ListaPartner.Add(new ListVariable { IdDest = 18, IdFunction = 999, DeFunction = "BF5" });

            //per 19 chidere non è chiaro
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 0, DeFunction = "CENTROVITA" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 111, DeFunction = "CONSEL" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 151, DeFunction = "FINDOMESTIC" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 196, DeFunction = "BARCLAYS" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 504, DeFunction = "AGOS" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 506, DeFunction = "CR FIRENZE" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 528, DeFunction = "COMPASS" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 531, DeFunction = "MICOS" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 532, DeFunction = "BNL" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 550, DeFunction = "UNICREDIT" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 555, DeFunction = "BNL FINANCE" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 599, DeFunction = "BANCA MARCHE" });
            ListaPartner.Add(new ListVariable { IdDest = 19, IdFunction = 600, DeFunction = "CARILO" });

            return ListaPartner;
        }



    }

    public class LoadVariable
    {
        public List<ListVariable> LoadAreaParameter(Int32 IdDestinatario)
        {
            List<ListVariable> Dati = new List<ListVariable>();

            var ListVariable = new ListVariable();
            var elenco = ListVariable.LoadFunzioniArea();

            var myDati = elenco.Where(o => o.IdDest == IdDestinatario);
            foreach(var Item in myDati)
            {

            Dati.Add(new ListVariable { IdDest = Item.IdDest, IdFunction = Item.IdFunction, DeFunction = Item.DeFunction });

            }
            return Dati;
        }
        public List<ListVariable> LoadPartnerParameter(Int32 IdDestinatario)
        {
            List<ListVariable> Dati = new List<ListVariable>();

            var ListVariable = new ListVariable();
            var elenco = ListVariable.LoadFunzioniPartner();

            var myDati = elenco.Where(o => o.IdDest == IdDestinatario);
            foreach (var Item in myDati)
            {
                Dati.Add(new ListVariable { IdDest = Item.IdDest, IdFunction = Item.IdFunction, DeFunction = Item.DeFunction });
            }
            return Dati;
        }


    }




}
