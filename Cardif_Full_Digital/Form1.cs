﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;

namespace Cardif_Full_Digital
{
    public partial class Form1 : Form
    {
        string applicazione;
        string percorso_file_protocollo = string.Empty;
        public Form1(string[] campi)
        {
            InitializeComponent();

//            string percorso_file_protocollo = string.Empty;

            //Leggo l'applicazione
            foreach (string c in campi)
            {
                //Escludo il percorso dove salvare il file con il protocollo
                if (c.IndexOf("\\") != -1)
                {
                    percorso_file_protocollo = c;
                    continue;
                }

                //Escludo i valori da assegnare ai campi
                if (c.IndexOf("=") != -1)
                    continue;

                applicazione = c.ToUpper();
                //break;
            }

            //Valorizzo il dictionary per inserire i parametri nei rispettivi campi
            Dictionary<string, string> campi_indicizzazione = Valorizza_campi_indicizzazione();

            //Valorizzo le combobox
            Valorizza_tipo_posta();
            Valorizza_destinatario();
            Valorizza_tipo_lettera();

            //Inserisco i valori predefiniti delle textbox
            Valorizza_data_ricevimento();

            //Leggo i valori passati e valorizzo i campi
            Valorizza_campi(campi_indicizzazione, campi);

            //Chiedo il nuovo protocollo
//            Chiedi_Protocollo(percorso_file_protocollo);
        }

        CardifClass.DatiContextClass myServiceDataClass = new CardifClass.DatiContextClass();

        private void Valorizza_tipo_posta()
        {
            #region POSTA
            cb_tipo_posta.Items.Add(string.Empty);
            cb_tipo_posta.Items.Add("Posta Ordinaria - 1");
            cb_tipo_posta.Items.Add("Raccomandata - 2");
            cb_tipo_posta.Items.Add("Fax - 3");
            cb_tipo_posta.Items.Add("Ricevuta Raccomandata - 4");
            cb_tipo_posta.Items.Add("Corriere - 7");
            cb_tipo_posta.Items.Add("Cons. a mano - 8");
            #endregion
            #region PNR
            cb_tipo_posta.Items.Add("Posta Ordinaria - 5");
            cb_tipo_posta.Items.Add("Raccomandata - 6");
            cb_tipo_posta.Items.Add("Corriere - 9");
            cb_tipo_posta.Items.Add("Cons. a mano - 10");
            #endregion
        }

        private void Valorizza_destinatario()
        {
            cb_destinatario.Items.Add(string.Empty);
            cb_destinatario.Items.Add("Gestione Risparmio - 1");
            cb_destinatario.Items.Add("Gestione Linea Persone - 12");
            cb_destinatario.Items.Add("Cqs - 18");
            cb_destinatario.Items.Add("Ufficio Sinistri - 19");
        }

        private void Valorizza_tipo_lettera()
        {
            cb_tipo_lettera.Items.Add(string.Empty);
            cb_tipo_lettera.Items.Add("Lettera NON Recapitata - 1");
//            cb_tipo_lettera.Items.Add("Posta Personale - 3");
        }

        private void cb_destinatario_TextChanged(object sender, EventArgs e)
        {
            //Rimuovo i valori della combobox CODICE_AREA e COD_PARTNER
            cb_codice_area.Items.Clear();
            cb_cod_partner.Items.Clear();

            //Pulisco i campi CODICE_AREA e COD_PARTNER
            cb_codice_area.Text = string.Empty;
            cb_cod_partner.Text = string.Empty;

            //Inserisco i valori  della combobox CODICE_AREA e COD_PARTNER in base al valore della combobox DESTINATARIO
            switch (cb_destinatario.Text)
            {
                case "Gestione Risparmio - 1":

                    #region CODICE_AREA
                    cb_codice_area.Items.Add(string.Empty);
                    cb_codice_area.Items.Add("RECLAMI - 12");
                    cb_codice_area.Items.Add("FPA-PARVEST - 16");
                    cb_codice_area.Items.Add("VITA_IDM - 19");
                    cb_codice_area.Items.Add("VITA_DIR TEC - 20");
                    cb_codice_area.Items.Add("VITA_DIR FIN - 21");
                    cb_codice_area.Items.Add("VITA_AFFARI SOCIETARI - 22");
                    cb_codice_area.Items.Add("VITA_SEGRETERIA GENERALE - 23");
                    cb_codice_area.Items.Add("VITA_RISORSE UMANE - 24");
                    cb_codice_area.Items.Add("PREVIDENZA - 27");
                    cb_codice_area.Items.Add("ATTI RISPARMIO - 28");
                    cb_codice_area.Items.Add("ATTI SAVING - 29");
                    cb_codice_area.Items.Add("RECLAMI RISPARMIO - 30");
                    cb_codice_area.Items.Add("RECLAMI SAVING - 31");
                    cb_codice_area.Items.Add("SAVING - 32");
                    #endregion

                    break;

                case "Gestione Linea Persone - 12":

                    #region CODICE_AREA
                    cb_codice_area.Items.Add(string.Empty);
                    cb_codice_area.Items.Add("ASSUNZIONE - DBS/MdA - 1");
                    cb_codice_area.Items.Add("SINISTRI - DENUNCIA - 2");
                    cb_codice_area.Items.Add("SINISTRI - SEGUITO - 3");
                    cb_codice_area.Items.Add("RECLAMI - 4");
                    cb_codice_area.Items.Add("POST VENDITA - 5");
                    cb_codice_area.Items.Add("ASSUNZIONE - QM/VM - 6");
                    cb_codice_area.Items.Add("SEGRETERIA GENERALE - 15");
                    cb_codice_area.Items.Add("FATTURE - 17");
                    cb_codice_area.Items.Add("ATTI DI CITAZIONE - 18");
                    cb_codice_area.Items.Add("CPI INDIVIDUALE FINDO - 25");
                    cb_codice_area.Items.Add("TCM FINDOMESTIC - 26");
                    #endregion
                    #region COD_PARTNER
                    cb_cod_partner.Items.Add(string.Empty);
                    cb_cod_partner.Items.Add("CENTROVITA - 0");
                    cb_cod_partner.Items.Add("CONSEL - 111");
                    cb_cod_partner.Items.Add("FINDOMESTIC - 151");
                    cb_cod_partner.Items.Add("BARCLAYS - 196");
                    cb_cod_partner.Items.Add("AGOS - 504");
                    cb_cod_partner.Items.Add("CR FIRENZE - 506");
                    cb_cod_partner.Items.Add("COMPASS - 528");
                    cb_cod_partner.Items.Add("MICOS - 531");
                    cb_cod_partner.Items.Add("BNL - 532");
                    cb_cod_partner.Items.Add("UNICREDIT - 550");
                    cb_cod_partner.Items.Add("BNL FINANCE - 555");
                    cb_cod_partner.Items.Add("BANCA MARCHE - 599");
                    cb_cod_partner.Items.Add("CARILO - 600");
                    #endregion

                    break;

                case "Cqs - 18":

                    #region CODICE_AREA
                    cb_codice_area.Items.Add("ASSUNZIONE - 7");
                    cb_codice_area.Items.Add("SINISTRI - 8");
                    cb_codice_area.Items.Add("RECUPERI - 9");
                    cb_codice_area.Items.Add("POST VENDITA - 10");
                    cb_codice_area.Items.Add("RECLAMI - 11");
                    #endregion
                    #region COD_PARTNER
                    cb_cod_partner.Items.Add("BNL FINANCE - 995");
                    cb_cod_partner.Items.Add("SIGLA - 996");
                    cb_cod_partner.Items.Add("MYTHOS - 997");
                    cb_cod_partner.Items.Add("CATTOLICA - 998");
                    cb_cod_partner.Items.Add("BF5 - 999");
                    #endregion

                    break;

                case "Ufficio Sinistri - 19":

                    #region CODICE_AREA
                    cb_codice_area.Items.Add("SINISTRI - DENUNCIA - 13");
                    cb_codice_area.Items.Add("SINISTRI – SEGUITO - 14");
                    #endregion
                    #region COD_PARTNER
                    cb_cod_partner.Items.Add("CENTROVITA - 0");
                    cb_cod_partner.Items.Add("CONSEL - 111");
                    cb_cod_partner.Items.Add("FINDOMESTIC - 151");
                    #endregion

                    break;
            }
        }

        private void Valorizza_data_ricevimento()
        {
            txt_dt_ricevimento.Text = DateTime.Now.ToShortDateString();
        }

        private void bt_richiesta_Click(object sender, EventArgs e)
        {
            if (Controlli())
            {

            }
        }

        private bool Controlli()
        {
            try
            {
                #region DT_RICEVIMENTO
                if (txt_dt_ricevimento.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo DT_RICEVIMENTO è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_dt_ricevimento.Focus();
                    return false;
                }
                else if (DateTime.ParseExact(txt_dt_ricevimento.Text.Trim(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) > DateTime.Now)
                {
                    MessageBox.Show("la data inserita nel campo DT_RICEVIMENTO non può essere nel futuro", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_dt_ricevimento.Focus();
                    return false;
                }
                else
                    myServiceDataClass.DtRicevimento = DateTime.Parse(txt_dt_ricevimento.Text);
                #endregion

                #region TIPO_POSTA
                if (cb_tipo_posta.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo TIPO_POSTA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_tipo_posta.Focus();
                    return false;
                }
                else
                    myServiceDataClass.TpPosta = CardifClass.Converter.ReturnValue(cb_tipo_posta.Text);


                    #endregion

                    #region DESTINATARIO
                    if (cb_destinatario.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo DESTINATARIO è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_destinatario.Focus();
                    return false;
                }
                else
                    myServiceDataClass.Destinatario = CardifClass.Converter.ReturnValue(cb_destinatario.Text);

                #endregion

                #region NUM_PAGINE
                if (!int.TryParse(txt_num_pagine.Text.Trim(), out int n))
                {
                    MessageBox.Show("il campo NUM_PAGINE deve essere valorizzato con un numero", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_num_pagine.Focus();
                    return false;
                }
                else
                    myServiceDataClass.NumPagine=CardifClass.Converter.ReturnValue(txt_num_pagine.Text);


                #endregion

                #region NUM_RACCOMANDATA
                if (new List<string>() { "Raccomandata - 2", "Ricevuta Raccomandata - 4", "Raccomandata - 6" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_num_raccomandata.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo NUM_RACCOMANDATA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_num_raccomandata.Focus();
                        return false;
                    }
                }
                else if (txt_num_raccomandata.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo NUM_RACCOMANDATA non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_num_raccomandata.Focus();
                    return false;
                }
                else
                    myServiceDataClass.Raccomandata = txt_num_raccomandata.Text.ToString();

                #endregion

                #region TIPO_LETTERA
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (cb_tipo_lettera.Text.Trim().Length != 0)
                    {
                        MessageBox.Show("il campo TIPO_LETTERA non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cb_tipo_lettera.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.TpLettera = CardifClass.Converter.ReturnValue(cb_tipo_lettera.Text);
                }
                else if (cb_tipo_lettera.Text.Trim().Length == 0)
                {
                    MessageBox.Show("il campo TIPO_LETTERA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_tipo_lettera.Focus();
                    return false;
                }
                else
                    myServiceDataClass.TpLettera = CardifClass.Converter.ReturnValue(cb_tipo_lettera.Text);
                #endregion

                #region MITTENTE
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_mittente.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo MITTENTE è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_mittente.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.Mittente = txt_mittente.Text;

                }
                else if (txt_mittente.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo MITTENTE non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_mittente.Focus();
                    return false;
                }
                else
                    myServiceDataClass.Mittente = txt_mittente.Text;

                #endregion

                    #region DT_INVIO
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_dt_invio.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo DT_INVIO è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_dt_invio.Focus();
                        return false;
                    }
                    else if (DateTime.ParseExact(txt_dt_invio.Text.Trim(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) > DateTime.Now)
                    {
                        MessageBox.Show("la data inserita nel campo DT_INVIO non può essere nel futuro", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_dt_invio.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.DtInvio = DateTime.Parse(txt_dt_invio.Text);
                }
                else if (txt_dt_invio.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo DT_INVIO non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_dt_invio.Focus();
                    return false;
                }
                #endregion

                #region CODICE_AREA
                if (new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (cb_codice_area.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("il campo CODICE_AREA è obbligatorio", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cb_codice_area.Focus();
                        return false;
                    }
                    else
                    {
                        myServiceDataClass.CodArea = CardifClass.Converter.ReturnValue(cb_codice_area.Text);
                    }
                }
                else if (cb_codice_area.Text.Trim().Length != 0)
                {
                    MessageBox.Show("il campo CODICE_AREA non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cb_codice_area.Focus();
                    return false;
                }


                #endregion

                #region COMMENTO
                if (!new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (txt_commento.Text.Trim().Length != 0)
                    {
                        MessageBox.Show("il campo COMMENTO non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txt_commento.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.Comment = txt_commento.Text;
                }
                #endregion

                #region COD_PARTNER
                if (!new List<string>() { "Posta Ordinaria - 1", "Raccomandata - 2", "Fax - 3", "Ricevuta Raccomandata - 4", "Corriere - 7", "Cons. a mano - 8" }.Contains(cb_tipo_posta.Text.Trim()))
                {
                    if (cb_cod_partner.Text.Trim().Length != 0)
                    {
                        MessageBox.Show("il campo COD_PARTNER non deve essere valorizzato", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cb_cod_partner.Focus();
                        return false;
                    }
                    else
                        myServiceDataClass.Partner = CardifClass.Converter.ReturnValue(cb_cod_partner.Text); 

                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private Dictionary<string, string> Valorizza_campi_indicizzazione()
        {
            Dictionary<string, string> campi = new Dictionary<string, string>();

            foreach (Control c in panel1.Controls)
            {
                if (c is Label)
                {
                    if (panel1.Controls.ContainsKey("txt_" + c.Text.ToLower()))
                        campi.Add(c.Text.ToLower(), "txt_" + c.Text.ToLower());
                    else if (panel1.Controls.ContainsKey("cb_" + c.Text.ToLower()))
                        campi.Add(c.Text.ToLower(), "cb_" + c.Text.ToLower());
                }
            }

            return campi;
        }

        private void Valorizza_campi(Dictionary<string, string> campi_indicizzazione, string[] campi)
        {
            foreach (string c in campi)
            {
                if (c.IndexOf("=") == -1)
                    continue;

                string[] campi_split = c.ToLower().Split('=');

                if (campi_indicizzazione.ContainsKey(campi_split[0]))
                    panel1.Controls.Find(campi_indicizzazione[campi_split[0]], true).First().Text = campi_split[1].ToUpper();
                else MessageBox.Show("Campo " + campi_split[0].ToUpper() + " non trovato!", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Log(string testo)
        {
            System.IO.Directory.CreateDirectory(System.IO.Path.Combine(System.Configuration.ConfigurationManager.AppSettings["Log"], DateTime.Now.ToString("yyyyMMdd")));
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(System.Configuration.ConfigurationManager.AppSettings["Log"], DateTime.Now.ToString("yyyyMMdd"), applicazione + ".txt"), true))
            {
                sw.WriteLine(DateTime.Now.ToString("yyyyMMdd_HHmmss") + "\t" + testo);
            }
        }
        private void Chiedi_Protocollo (string percorso)
        {
            string nprotocollo = string.Empty;

            try
            {

                //System.ServiceModel.Channels.Binding binding = new BasicHttpBinding();
                //WSHttpBinding b = new WSHttpBinding();
                //b.Security.Mode = SecurityMode.Transport;
                //b.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

                //TODO
                //GESTIONE Web Service
                //BasicHttpBinding mybinding = new BasicHttpBinding();
                //mybinding.Security.Mode = BasicHttpSecurityMode.Transport;
                //mybinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                //mybinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
                //mybinding.ReaderQuotas.MaxStringContentLength = 5242880;
                //mybinding.MaxReceivedMessageSize = 5242880;
                //mybinding.MaxBufferSize = 5242880;
                //// setting timeouts 
                //string TimeOut = "60";
                //Double dTout = Convert.ToDouble(TimeOut);
                //mybinding.OpenTimeout = TimeSpan.FromSeconds(dTout);
                //mybinding.CloseTimeout = TimeSpan.FromSeconds(dTout);
                //mybinding.SendTimeout = TimeSpan.FromSeconds(dTout);
                //mybinding.ReceiveTimeout = TimeSpan.FromSeconds(dTout);

    
                using (WebServiceCardifnew.PPWS2ProxyServiceClient WsCardif = new WebServiceCardifnew.PPWS2ProxyServiceClient())
                {

                    System.Net.ServicePointManager.ServerCertificateValidationCallback +=(sender, cert, chain, error) => {
                        return true;
                    };


                    string CardifUrl = Properties.Settings.Default.myUrl.ToString();
                    WsCardif.Endpoint.Address = new System.ServiceModel.EndpointAddress(CardifUrl);

                    //                    WsCardif.Endpoint.Address = new System.ServiceModel.EndpointAddress("https://192.168.62.51/9029/service/protocollazionePosta/v1");
                   
                    WsCardif.ClientCredentials.UserName.UserName = "techusr_microdata";
                    WsCardif.ClientCredentials.UserName.Password = "techusr_microdata";

                    //                    WsCardif.ClientCredentials.ServiceCertificate.SetDefaultCertificate(StoreLocation.LocalMachine, StoreName.My, X509FindType.FindBySubjectName, "mioCert");

                    WebServiceCardifnew.getNumProtocolRequest dati = new WebServiceCardifnew.getNumProtocolRequest();
                    //dati.
                    dati.DT_RICEVIMENTO = myServiceDataClass.DtRicevimento;
                    dati.CODICE_AREA = myServiceDataClass.CodArea;
                    dati.TIPO_POSTA = myServiceDataClass.TpPosta;
                    dati.DESTINATARIO = myServiceDataClass.Destinatario;
                    dati.NUM_PAGINE = myServiceDataClass.NumPagine;
                    dati.NUM_RACCOMANDATA = myServiceDataClass.Raccomandata;
                    dati.TIPO_LETTERA = myServiceDataClass.TpLettera;
                    dati.MITTENTE = myServiceDataClass.Mittente;
                    dati.DT_INVIO = myServiceDataClass.DtInvio;
                    dati.COMMENTO = myServiceDataClass.Comment;
                    dati.COD_PARTNER = myServiceDataClass.Partner;

                    WebServiceCardifnew.SecurityInfoTypeUserInfo WsLogin = new WebServiceCardifnew.SecurityInfoTypeUserInfo();
 //                   WsCardif.Open();
                    WsLogin.Login = "techusr_microdata";
                    WsLogin.Password = "techusr_microdata";

                    WebServiceCardifnew.HeaderInput WsHeader = new WebServiceCardifnew.HeaderInput();

                    WsHeader.Header = new WebServiceCardifnew.HeaderInputType();
                    WsHeader.Header.ContextPath = "";
//                    WsHeader.Header.CorrelationId = "";
                    WsHeader.Security = new WebServiceCardifnew.SecurityInfoType();
                    WsHeader.Security.ApplicationId="";
                    WsHeader.Security.Item = WsLogin;

                    WebServiceCardifnew.getNumProtocolResult result = WsCardif.getNumProtocol(WsHeader, dati);

                    Int32 Protocollo = result.NUMPROTOCOL;
//                    WsCardif.Close();
 
                    //result.
                    string ApplicationID = Guid.NewGuid().ToString();

                        if (Protocollo < 0)  // minore di 0 segnala un errore
                        {
                            string msgError = CardifClass.RetriveError.ReturnMsgError(result.NUMPROTOCOL);
                            //Mostra il messaggio di errore
                            MessageBox.Show(msgError, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else if (string.IsNullOrEmpty(result.ERRORS) == false)  // se diversa da null segnala 1 o più errori sulla stessa stringa
                        {
                            string[] myError = Regex.Split(result.ERRORS, @"[^;]*");
                            //Mostra l'elenco dei problemi riscontrati
                            if (myError.Count() > 0)
                            {
                                for (int i = 0; i < myError.Count(); i++)
                                {
                                    MessageBox.Show(myError[i].ToString(), "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                            }
                        }
                        else
                        {
                          //  Int32 nProtocollo = result.NUMPROTOCOL;
                            //label12.Text = nProtocollo.ToString();

                            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(percorso, "Protocollo.txt")))
                                sw.Write(Protocollo);
                            Clipboard.SetText(Protocollo.ToString());
                            ClearFields();
                        }
                    if (!string.IsNullOrEmpty(result.ERRORS)) // se result.ERRORS = empty nessun errore
                    {
                        string[] myError = Regex.Split(result.ERRORS, @"(;)");
                        //Mostra l'elenco dei problemi riscontrati
                        if (myError.Count() > 0)
                        {
                            for (int i = 0; i < myError.Count(); i++)
                            {
//                                MessageBox.Show(myError[i].ToString(), "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }

                    }
                }
           
            }
            catch(Exception ex)
            {

                MessageBox.Show(ex.Message, "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log(ex.Message);

                //Metto l'errore come se l'avesse copiato l'operatore
                Clipboard.SetText("ERRORE");
            }
        }
        private void ClearFields()
        {
            if(cb_tipo_posta.Items.Count > 0)
                cb_tipo_posta.SelectedIndex = 0;

            if (cb_destinatario.Items.Count > 0)
                cb_destinatario.SelectedIndex = 0;

                txt_num_pagine.Text = string.Empty;
                txt_num_raccomandata.Text = string.Empty;

            if (cb_tipo_lettera.Items.Count > 0)
                cb_tipo_lettera.SelectedIndex = 0;

                txt_mittente.Text = string.Empty;
                txt_dt_invio.Text = string.Empty;

            if (cb_codice_area.Items.Count > 0)
                cb_codice_area.SelectedIndex = 0;

                txt_commento.Text = string.Empty;

            if (cb_cod_partner.Items.Count > 0)
                cb_cod_partner.SelectedIndex = 0;
        }

        private void bt_richiesta_Click_1(object sender, EventArgs e)
        {
            bool _ControlOk = Controlli();

            if (_ControlOk == true)
            {

                Chiedi_Protocollo(percorso_file_protocollo);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AfterSelected(object sender, EventArgs e)
        {
            string _nomeCombo = ((ComboBox)sender).Name.ToLower();

            switch (_nomeCombo)
            {
                case  "cb_tipo_posta":
                    cb_destinatario.Focus();
                    break;
                case "cb_destinatario":
                    txt_num_pagine.Focus();
                    break;
                case "cb_tipo_lettera":
                    txt_mittente.Focus();
                    break;
            }

        }
    }
}
