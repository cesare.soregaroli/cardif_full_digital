﻿namespace Cardif_Full_Digital
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Close = new System.Windows.Forms.Button();
            this.CmbNPag = new System.Windows.Forms.ComboBox();
            this.TxtError = new System.Windows.Forms.TextBox();
            this.TxtProtocol = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.bt_richiesta = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cb_cod_partner = new System.Windows.Forms.ComboBox();
            this.txt_commento = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cb_codice_area = new System.Windows.Forms.ComboBox();
            this.txt_dt_invio = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_mittente = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_tipo_lettera = new System.Windows.Forms.ComboBox();
            this.txt_num_raccomandata = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_num_pagine = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_destinatario = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_tipo_posta = new System.Windows.Forms.ComboBox();
            this.txt_dt_ricevimento = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.CmbNPag);
            this.panel1.Controls.Add(this.TxtError);
            this.panel1.Controls.Add(this.TxtProtocol);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.bt_richiesta);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.cb_cod_partner);
            this.panel1.Controls.Add(this.txt_commento);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.cb_codice_area);
            this.panel1.Controls.Add(this.txt_dt_invio);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txt_mittente);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cb_tipo_lettera);
            this.panel1.Controls.Add(this.txt_num_raccomandata);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txt_num_pagine);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cb_destinatario);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cb_tipo_posta);
            this.panel1.Controls.Add(this.txt_dt_ricevimento);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(442, 457);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btn_Close
            // 
            this.btn_Close.AutoSize = true;
            this.btn_Close.Location = new System.Drawing.Point(16, 412);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(78, 33);
            this.btn_Close.TabIndex = 51;
            this.btn_Close.Text = "Chiudi";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Visible = false;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // CmbNPag
            // 
            this.CmbNPag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbNPag.FormattingEnabled = true;
            this.CmbNPag.Items.AddRange(new object[] {
            "1",
            "33",
            "66",
            "77",
            "90",
            "99",
            " "});
            this.CmbNPag.Location = new System.Drawing.Point(146, 118);
            this.CmbNPag.Name = "CmbNPag";
            this.CmbNPag.Size = new System.Drawing.Size(281, 21);
            this.CmbNPag.TabIndex = 4;
            // 
            // TxtError
            // 
            this.TxtError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtError.Location = new System.Drawing.Point(146, 342);
            this.TxtError.Multiline = true;
            this.TxtError.Name = "TxtError";
            this.TxtError.Size = new System.Drawing.Size(281, 64);
            this.TxtError.TabIndex = 50;
            // 
            // TxtProtocol
            // 
            this.TxtProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtProtocol.Location = new System.Drawing.Point(146, 309);
            this.TxtProtocol.Name = "TxtProtocol";
            this.TxtProtocol.ReadOnly = true;
            this.TxtProtocol.Size = new System.Drawing.Size(174, 20);
            this.TxtProtocol.TabIndex = 49;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 368);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 48;
            this.label14.Text = "errore....";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 311);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 47;
            this.label13.Text = "n Protocollo...";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 316);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 46;
            // 
            // bt_richiesta
            // 
            this.bt_richiesta.AutoSize = true;
            this.bt_richiesta.Location = new System.Drawing.Point(260, 412);
            this.bt_richiesta.Name = "bt_richiesta";
            this.bt_richiesta.Size = new System.Drawing.Size(167, 33);
            this.bt_richiesta.TabIndex = 45;
            this.bt_richiesta.Text = "Richiedi numero protocollazione";
            this.bt_richiesta.UseVisualStyleBackColor = true;
            this.bt_richiesta.Click += new System.EventHandler(this.bt_richiesta_Click_1);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 279);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "COD_PARTNER";
            // 
            // cb_cod_partner
            // 
            this.cb_cod_partner.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_cod_partner.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_cod_partner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_cod_partner.FormattingEnabled = true;
            this.cb_cod_partner.Location = new System.Drawing.Point(146, 276);
            this.cb_cod_partner.Name = "cb_cod_partner";
            this.cb_cod_partner.Size = new System.Drawing.Size(281, 21);
            this.cb_cod_partner.TabIndex = 10;
            // 
            // txt_commento
            // 
            this.txt_commento.Location = new System.Drawing.Point(146, 250);
            this.txt_commento.MaxLength = 50;
            this.txt_commento.Name = "txt_commento";
            this.txt_commento.Size = new System.Drawing.Size(281, 20);
            this.txt_commento.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 253);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "COMMENTO";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "CODICE_AREA";
            // 
            // cb_codice_area
            // 
            this.cb_codice_area.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_codice_area.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_codice_area.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_codice_area.FormattingEnabled = true;
            this.cb_codice_area.Location = new System.Drawing.Point(146, 223);
            this.cb_codice_area.Name = "cb_codice_area";
            this.cb_codice_area.Size = new System.Drawing.Size(281, 21);
            this.cb_codice_area.TabIndex = 8;
            this.cb_codice_area.SelectedIndexChanged += new System.EventHandler(this.AfterSelected);
            // 
            // txt_dt_invio
            // 
            this.txt_dt_invio.Location = new System.Drawing.Point(146, 197);
            this.txt_dt_invio.MaxLength = 10;
            this.txt_dt_invio.Name = "txt_dt_invio";
            this.txt_dt_invio.Size = new System.Drawing.Size(281, 20);
            this.txt_dt_invio.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "DT_INVIO";
            // 
            // txt_mittente
            // 
            this.txt_mittente.Location = new System.Drawing.Point(146, 171);
            this.txt_mittente.MaxLength = 200;
            this.txt_mittente.Name = "txt_mittente";
            this.txt_mittente.Size = new System.Drawing.Size(281, 20);
            this.txt_mittente.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "MITTENTE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "TIPO_LETTERA";
            // 
            // cb_tipo_lettera
            // 
            this.cb_tipo_lettera.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_tipo_lettera.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_tipo_lettera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipo_lettera.FormattingEnabled = true;
            this.cb_tipo_lettera.Location = new System.Drawing.Point(146, 37);
            this.cb_tipo_lettera.Name = "cb_tipo_lettera";
            this.cb_tipo_lettera.Size = new System.Drawing.Size(281, 21);
            this.cb_tipo_lettera.TabIndex = 1;
            this.cb_tipo_lettera.SelectedIndexChanged += new System.EventHandler(this.AfterSelected);
            // 
            // txt_num_raccomandata
            // 
            this.txt_num_raccomandata.Location = new System.Drawing.Point(146, 145);
            this.txt_num_raccomandata.MaxLength = 13;
            this.txt_num_raccomandata.Name = "txt_num_raccomandata";
            this.txt_num_raccomandata.Size = new System.Drawing.Size(281, 20);
            this.txt_num_raccomandata.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "NUM_RACCOMANDATA";
            // 
            // txt_num_pagine
            // 
            this.txt_num_pagine.Location = new System.Drawing.Point(146, 419);
            this.txt_num_pagine.Name = "txt_num_pagine";
            this.txt_num_pagine.Size = new System.Drawing.Size(48, 20);
            this.txt_num_pagine.TabIndex = 30;
            this.txt_num_pagine.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "NUM_PAGINE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "DESTINATARIO";
            // 
            // cb_destinatario
            // 
            this.cb_destinatario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_destinatario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_destinatario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_destinatario.FormattingEnabled = true;
            this.cb_destinatario.Location = new System.Drawing.Point(146, 65);
            this.cb_destinatario.Name = "cb_destinatario";
            this.cb_destinatario.Size = new System.Drawing.Size(281, 21);
            this.cb_destinatario.TabIndex = 2;
            this.cb_destinatario.SelectedIndexChanged += new System.EventHandler(this.cb_destinatario_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "TIPO_POSTA";
            // 
            // cb_tipo_posta
            // 
            this.cb_tipo_posta.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_tipo_posta.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_tipo_posta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipo_posta.FormattingEnabled = true;
            this.cb_tipo_posta.Location = new System.Drawing.Point(146, 92);
            this.cb_tipo_posta.Name = "cb_tipo_posta";
            this.cb_tipo_posta.Size = new System.Drawing.Size(281, 21);
            this.cb_tipo_posta.TabIndex = 3;
            this.cb_tipo_posta.SelectedIndexChanged += new System.EventHandler(this.AfterSelected);
            // 
            // txt_dt_ricevimento
            // 
            this.txt_dt_ricevimento.Location = new System.Drawing.Point(146, 9);
            this.txt_dt_ricevimento.MaxLength = 10;
            this.txt_dt_ricevimento.Name = "txt_dt_ricevimento";
            this.txt_dt_ricevimento.Size = new System.Drawing.Size(281, 20);
            this.txt_dt_ricevimento.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "DT_RICEVIMENTO";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 457);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CARDIF FULL DIGITAL";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bt_richiesta;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cb_cod_partner;
        private System.Windows.Forms.TextBox txt_commento;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cb_codice_area;
        private System.Windows.Forms.TextBox txt_dt_invio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_mittente;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cb_tipo_lettera;
        private System.Windows.Forms.TextBox txt_num_raccomandata;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_num_pagine;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_destinatario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_tipo_posta;
        private System.Windows.Forms.TextBox txt_dt_ricevimento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtProtocol;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtError;
        private System.Windows.Forms.ComboBox CmbNPag;
        private System.Windows.Forms.Button btn_Close;
    }
}

